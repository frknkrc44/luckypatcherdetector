package org.frknkrc44.pdetect;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import java.util.List;

public class LuckyPatcherDetector {
	
	private LuckyPatcherDetector(){}
	
	public static boolean isFound(Context ctx){
		return getLuckyPatcher(ctx) != null;
	}
	
	public static ApplicationInfo getLuckyPatcher(Context ctx){
		PackageManager pm = ctx.getPackageManager();
		List<ApplicationInfo> pkgs = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		for(ApplicationInfo pkg : pkgs){
			Bundle meta = pkg.metaData;
			if(meta != null){
				String str = meta.getString("xposeddescription");
				if(str != null && str.contains("Lucky") && str.contains("Patcher")){
					return pkg;
				}
			}
		}
		return null;
	}
	
}
