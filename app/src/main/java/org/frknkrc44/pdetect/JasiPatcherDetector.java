package org.frknkrc44.pdetect;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import java.util.List;

public class JasiPatcherDetector {

	private JasiPatcherDetector(){}

	public static boolean isFound(Context ctx){
		return getJasiPatcher(ctx) != null;
	}

	public static ApplicationInfo getJasiPatcher(Context ctx){
		PackageManager pm = ctx.getPackageManager();
		List<ApplicationInfo> pkgs = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		for(ApplicationInfo pkg : pkgs){
			Bundle meta = pkg.metaData;
			if(meta != null){
				String str = meta.getString("xposeddescription");
				if(str != null && str.contains("Jasi")){
					return pkg;
				}
			}
		}
		return null;
	}

}
