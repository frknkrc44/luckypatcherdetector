package org.frknkrc44.pdetect;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onRestart();
		TextView text = new TextView(this);
		text.setLayoutParams(new FrameLayout.LayoutParams(-1,-1));
		text.setGravity(Gravity.CENTER);
		ApplicationInfo lp = LuckyPatcherDetector.getLuckyPatcher(this);
		ApplicationInfo jp = JasiPatcherDetector.getJasiPatcher(this);
		String lpf = String.format("LP %s", lp != null ? "FOUND: " + lp.packageName : "not found");
		String jpf = String.format("JP %s", jp != null ? "FOUND: " + jp.packageName : "not found"); 
		text.setText(String.format("%s\n%s", lpf, jpf));
		setContentView(text);
	}
}
